package com.floriano.rafael.moviestowatchwithyourlove;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoviesToWatchWithYourLoveApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoviesToWatchWithYourLoveApplication.class, args);
	}

}
